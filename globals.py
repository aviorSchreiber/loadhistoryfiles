#### Globals #####
nodesDescription = {"origin":"pickupAddress",
        "port_0":"portOfLoading",
        "port_1":"portOfDischarge",
        "destination":"deliveryAddress"
        }
destCharge = ["destinationDetails","serviceItems"]
origCharge = ["originDetails","serviceItems"]
shippingInfo =[
    "tenantId",
    "id",
    "importExport",
    "type",
    "mode",
    "incoTerms",
    "totalPieces"
]
