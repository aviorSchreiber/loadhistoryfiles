import gremlin_python
from gremlin_python import statics
from gremlin_python.process.anonymous_traversal import traversal
from gremlin_python.process.graph_traversal import __
from gremlin_python.process.strategies import *
from gremlin_python.driver.driver_remote_connection import DriverRemoteConnection
from gremlin_python.process.traversal import T
from gremlin_python.process.traversal import Order
from gremlin_python.process.traversal import Cardinality
from gremlin_python.process.traversal import Column
from gremlin_python.process.traversal import Direction
from gremlin_python.process.traversal import Operator
from gremlin_python.process.traversal import P
from gremlin_python.process.traversal import Pop
from gremlin_python.process.traversal import Scope
from gremlin_python.process.traversal import Barrier
from gremlin_python.process.traversal import Bindings
from gremlin_python.process.traversal import WithOptions
from gremlin_python.structure.graph import Graph
from functions import *
from globals import *


class LoadFile:
    def __init__(self,neptune_url,bucketName,fileName):
        self.neptune_url=neptune_url
        self.bucketName=bucketName
        self.fileName=fileName
    @classmethod
    def start(cls,neptune_url,bucketName,fileName):
        cls.neptune_url=neptune_url
        cls.bucketName=bucketName
        cls.fileName=fileName
        data=getDataFromBucket(cls.bucketName,cls.fileName)
        data=initialData(data)
        connection = 'ws://' + cls.neptune_url + ':8182/gremlin'
        graph = Graph()
        drc = DriverRemoteConnection(connection,'g',max_workers=1)
        g = graph.traversal().withRemote(drc)
        tenant=data['tenantId']
        sid=data['id']
        #S1 - Creating Shipping Node
        try:
            sid,sinfo,shipping=createShippingNode(shippingInfo,tenant,data,g)
        except Exception as e:
            if "Vertex with id already exists" in e.__str__():
                drc.close()
                return {"success": False,'status':'failed','statusCode': 409 , 'reason':'shipping Id {0} is exist!'.format(sid)}
            else: 
                print("[Error][createShippingNode] {}".format(e.__str__()))
                drc.close()
                return e.__str__()

        #S2 - Createing Node Description Objects
        nodes = []
        for k,v in nodesDescription.items():
            eta = None
            etd = None
            if 'eta' in data[v]:
                eta = data[v]['eta']
                del data[v]['eta']
            if 'etd' in data[v]:
                etd = data[v]['etd']
                del data[v]['etd']
            if v == "pickupAddress":
                if "id" not in data[v] and "erpId" in data[v] and data[v]['erpId'] != None:
                    data[v]['id']=data[v]['erpId']
                else:
                    try:
                        data[v]['id']="{code}_{city}_{name}".format(code=str(data[v]['postalCode']).replace(" ","_"),city=data[v]['city'],name=data[v]['name'].replace(" ","_"))
                    except:
                        continue
                ver = addVertex( k , tenant,data[v],g,objectType="Door")
            elif v == "deliveryAddress":
                if "id" not in data[v] and "erpId" in data[v] and data[v]['erpId'] != None:
                    data[v]['id']=data[v]['erpId']
                else:
                    try:
                        data[v]['id']="{code}_{city}_{name}".format(code=str(data[v]['postalCode']).replace(" ","_"),city=data[v]['city'],name=data[v]['name'].replace(" ","_"))
                    except:
                        continue
                ver = addVertex( k , tenant,data[v],g,objectType="Door")
                
            elif v == "portOfLoading":
                if "id" not in data[v] and "erpId" in data[v] and data[v]['erpId'] != None:
                    data[v]['id']=data[v]['erpId']
                else:
                    try:
                        data[v]['id']="{code}_{city}_{name}".format(code=data[v]['code'],city=data[v]['city'],name=data[v]['name'].replace(" ","_"))
                    except:
                        continue
                ver = addVertex( k , tenant,data[v],g,objectType="Port")
            elif v == "portOfDischarge":
                if "id" not in data[v] and "erpId" in data[v] and data[v]['erpId'] != None:
                    data[v]['id']=data[v]['erpId']
                else:
                    try:
                        data[v]['id']="{code}_{city}_{name}".format(code=data[v]['code'],city=data[v]['city'],name=data[v]['name'].replace(" ","_"))
                    except:
                        continue
                ver = addVertex( k , tenant,data[v],g,objectType="Port")
            else:
                print(k,v)
            nodes.append(ver)
            # keep shipping id data
            props={'sid': sid}
            if eta is not None: 
                props['eta'] = eta
            if etd is not None: 
                props['etd'] = etd
            #Create relationship between shipping Node and description Node the relationship is "Route"
            addEdge('route',shipping,ver,g,props) 
            del data[v]
        parsingPorts(g,nodes,sinfo,props)


        #S3 - Creating Consignee Or Shipper Object
        if 'id' in data['shipper'] and data['shipper']['id']!= None:
            shipper_id,shipper,shipperV = createShipperConsignee(tenant,data,g,shipping,'shipper')
            addShipperConsigneeId(g,shipperV,shipper_id,'customerId')

        if 'id' in data['consignee'] and data['consignee']['id']!= None:    
            consignee_id,consignee,consigneeV = createShipperConsignee(tenant,data,g,shipping,'consignee')
            addShipperConsigneeId(g,consigneeV,consignee_id,'customerId')


        #S4 - Creating Main Freight Object
        hazardousCount,freightCount=createMainFreight(tenant,data,g,shipping)
        if hazardousCount !=0:
            if hazardousCount==freightCount : 
                g.V(shipping).property('hazardous','Hazardous').next()
            else:  
                g.V(shipping).property('hazardous','Both').next()
        else:  
            g.V(shipping).property('hazardous','General Cargo').next()

        #S5 - Creating Chargees Orgin/Dest
        if "destinationDetails" in data:
            chargeListDest = data[destCharge[0]][destCharge[1]]
            parseChargeList(tenant,chargeListDest,data,shipping,g) 
            del data[destCharge[0]][destCharge[1]]
        if "originDetails" in data:
            chargeListOrigin = data[origCharge[0]][origCharge[1]]
            parseChargeList(tenant,chargeListOrigin,data,shipping,g)
            del data[origCharge[0]][origCharge[1]]


        #S6 - Setting Shipping Properties
        freightAttributesToShipping(data,g,shipping)
        for k,v in flattenDict(data).items():
            g.V(shipping).property(k,normalizeValue(v)).next()

        drc.close()
        return {"success": True,'status':'ok','statusCode': 200 , 'reason':None}
