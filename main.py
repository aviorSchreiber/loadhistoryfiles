import gremlin_python
from Classes.LoadFile import *
from Classes.ThreadWithReturnValue import *
from globals import *

import boto3
import json
import os
import time

maxThreads=int(os.getenv("maxThreads"))
if not maxThreads:
    maxThreads=1
revMaxThreads=list(range(maxThreads))
revMaxThreads.reverse()
sqs = boto3.resource('sqs','eu-west-1')
SQSURL=os.getenv("SQSURL")#'https://sqs.eu-west-1.amazonaws.com/896629681441/firstSqs'
neptune_url = os.getenv("neptuneUrl")
messages_to_delete=[]
queue=sqs.Queue(url=SQSURL)
start=time.time()
availableThreads=maxThreads
print("starting App \nneptuneUrl={}\nSQSURL={}\nmaxThreads={}".format(neptune_url,SQSURL,maxThreads))
while True:
    messages_to_delete = []
    threads=[]
    results=[]
    errors=[]
    messages=queue.receive_messages(MaxNumberOfMessages=10)
    for message in messages:
        # process message body
        body = json.loads(message.body)
        messages_to_delete.append({
            'Id': message.message_id,
            'ReceiptHandle': message.receipt_handle
        })
        try:
            bucketName=body['detail']['requestParameters']['bucketName']
            fileName=body['detail']['requestParameters']['key']
        except:
            continue
        if not bucketName:
            continue
        if len(messages)>5 and len(messages) > maxThreads and maxThreads>1 and availableThreads > 0:
            #Multi Thread for Many Reasults
            t=ThreadWithReturnValue(target=LoadFile.start,args=(neptune_url,bucketName,fileName,))
            threads.append(t)
            t.start()
            availableThreads-=1
        else:
            res=LoadFile.start(neptune_url,bucketName,fileName)
            if not res['success']:
                print("E-{}".format(res))
            else:
                print("R-{}".format(res))
    if len(messages)>6 and len(messages)>maxThreads and maxThreads>1 and availableThreads == 0:
        for thread in revMaxThreads:
            results.append(threads[thread-1].join())
        for thread in revMaxThreads:
            if results[thread-1]['success'] != True:
                errors.append(threads[thread-1])
            else:
                print("M-{}".format(results[thread-1]))
            availableThreads+=1
            del threads[thread-1]
  
        if len(errors)>0:
            print("ME-{}".format(errors))
    # if you don't receive any notifications the
    # messages_to_delete list will be empty
    if len(messages_to_delete) == 0:
        print("Done in {}".format(time.time()-start))
        time.sleep(60*1)
        start=time.time()
    # delete messages to remove them from SQS queue
    # handle any errors
    else:
        delete_response = queue.delete_messages(
                Entries=messages_to_delete)

