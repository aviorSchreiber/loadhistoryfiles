from globals import *
from gremlin_python.process.traversal import T
import dateutil.parser
import time
import re
import boto3
import json

def getDataFromBucket(bucketName,fileName):
    client = boto3.client('s3')
    obj = client.get_object(Bucket=bucketName, Key=fileName)
    file=obj['Body'].read().decode("utf-8")
    data=json.loads(file)
    return data 
    
def normalizeValue(val=None):
    """
    return datetime object if found.
    """
    matcher =re.compile(r'^\d{4}-\d{2}-\d{2}[T]\d{2}:\d{2}.*')
    if type(val) is str and matcher.match(val)!=None : 
        return dateutil.parser.parse(val)
    return val;
    
def flattenDict( _dict,prefix=None):
    """
    return flat Dictinary for complex dict
    Exmaple: {
        'address':
                    {
                    'address1':'dijon',
                    'name':'Peugeot',
                    'countryCode': 'FR'
                    },
        'id':1
        }
    return {
        'address.address1':'digon',
        'address.name':'peugeot',
        'address.counrtCode':'FR'
        'id':1
        }
    """
    badkeys = []
    newkeys = {}
    for key,val in _dict.items():
        if  val !=None and type(val).__name__ == "dict":
            val = flattenDict(val,key)
            badkeys.append(key)
            newkeys = {**newkeys,**val}  
        else:
            if val == None  or prefix != None :
                badkeys.append(key)
                if val!=None : newkeys[key] = val
    for key in badkeys:
        del _dict[key]
    for k,v in newkeys.items():
        key =k
        if prefix != None : key = prefix + "." + k
        if v !=None :  _dict[key]=v
    return _dict

def addEdge(lable,v1,v2,g,props={}) :
    """
    Adding relationship between to vertex and give the label
    """
    if props==None:
        return
    
    e = g.addE(lable).from_(v1).to(v2).property('insert_time',str(int(round(time.time() * 1000)))).next()
    
    for key, value in props.items():
        if value!=None :
            g.E(e).property(key,normalizeValue(value)).next()

def addVertex( label, tenant, props,g, checkExists=True,objectType=None):
    props=flattenDict(props)
    if checkExists:
        if g.V(props['id']).count().next() >0:   
            return g.V(props['id']).next()
    
    # create new vertex with all properties
    now = str(int(round(time.time() * 1000)))
    if checkExists or label=="shipping":
        v = g.addV(label).property(T.id,props['id']).property('tenantId',tenant).property('insertTime', now)
    else:
        v = g.addV(label).property('tenantId',tenant).property('insertTime', now)
    for key, value in props.items():
        if value != None:
            v.property(key, normalizeValue(value))
    if label != "shipping":
        try:
            return v.next()
        except:
            return g.V(props['id']).next()
    else:
        return v.next()

def createShippingNode(shippingInfo,tenant,data,g):
    sinfo ={}
    for key in shippingInfo:
        if data[key] !=None :
            sinfo[key]= data[key]
            del data[key]
    shipping = addVertex('shipping',tenant, sinfo,g, False,objectType="shipping")
    # get shipping id from db by shipping
    sid = sinfo['id']
    return sid,sinfo,shipping

def createShipperConsignee(tenant,data,g,shipping,ObjectType):
    """
    This Function is for craeting shipper consignee objects
    return: id, flat object and vertex object 
    """
    Object = data[ObjectType]
    Object = flattenDict(Object)
    if 'id' in Object : 
        id = Object['id']
    ObjectV  = addVertex('customer',tenant,Object,g,objectType=ObjectType)
    addEdge(ObjectType,shipping, ObjectV,g)
    del data[ObjectType]
    return id,Object,ObjectV

def addShipperConsigneeId(g,ObjectV,ObjectId,objectType):
    if g.V(ObjectV).properties(objectType).count().next()==0: 
        g.V(ObjectV).property(objectType,ObjectId).next()

def setPortType(node,mode,g):
    """
    Set the port Type
    """
    port = 'AIR_PORT'
    if mode=='SEA' : port = 'SEA_PORT'
        
    g.V(node).property('portType',port).next()

def parsingPorts(g,nodes,sinfo,props):
    lengthNodeList=len(nodes)
    for j in range(lengthNodeList):
        if( j !=0 and j!=lengthNodeList-1 ): # we are checking if the node is not the sender or the delivery
            g.V(nodes[j]).property('type','PORT').next()
            setPortType(nodes[j],sinfo['mode'],g)
        else:
            g.V(nodes[j]).property('type','DOOR').next()
        if (j<lengthNodeList-1):
            addEdge('to',nodes[j],nodes[j+1],g,props)


def createMainFreight(tenant,data,g,shipping):
    """
    create main freight by freight list 
    """
    freightItems = ["mainFreight","freightPieces"]
    hazardousCount=0
    freightCount = 0
    freightList = data[freightItems[0]][freightItems[1]]
    if freightList !=None :
        freightCount = len(freightList)
        for freight in freightList:
            _freight = flattenDict(freight)
            if (_freight.get('hazardClass') is not None) and (_freight.get('hazardClass').lower().startswith('class')) and  (re.search(r'\d+',  _freight.get('hazardClass')) !=None): 
                _freight['hazardClassMain'] =  re.search(r'\d+',  _freight.get('hazardClass')).group()

            if _freight.get('hazardous') is not None and _freight.get('hazardous'):
                hazardousCount=hazardousCount+1
            v  = addVertex('freight',tenant, _freight,g,False)
            addEdge('freight',shipping,v,g)
        return hazardousCount,freightCount

def freightAttributesToShipping(data,g,shipping):
    freights = ["mainFreight","freight"]
    freightItems = ["mainFreight","freightPieces"]
    
    freight =  data[freights[0]][freights[1]]
    freight = flattenDict(freight,'mainFreight')
    for k,v in freight.items():
        #we adding the freight details to shipping Vertex
        g.V(shipping).property(k,normalizeValue(v)).next()

    freightList = data[freightItems[0]][freightItems[1]]
    if freightList !=None and (not 'totalNoOfPieces' in freight or freight['totalNoOfPieces'] is None):
        freight['totalNoOfPieces']=len(freightList)

    del data[freightItems[0]][freightItems[1]]

def getMonetaryData(props):            
    """
    Get Charge dict 
    return: dict of cost (rate * quantity) and revenue (rate * quantity)
    """
    rate=0
    if "serviceCost.ratePerUnitConverted" in props and float( props["serviceCost.ratePerUnitConverted"] )>0 :
        rate =float( props["serviceCost.ratePerUnitConverted"] )
    elif "serviceCost.ratePerUnit" in props :
        rate = float(props["serviceCost.ratePerUnit"] )
    if rate==None : rate = 0

    quantity = 0
    if "serviceCost.numberOfUnits" in props:            
        quantity = float(props["serviceCost.numberOfUnits"] )
    if (quantity==None) : quantity =0
    
    cost = rate *  quantity
    # get revenue
    rate=0
    if "serviceRevenue.ratePerUnitConverted" in props and float( props["serviceRevenue.ratePerUnitConverted"] ) > 0: 
        rate =float( props["serviceRevenue.ratePerUnitConverted"] )
    elif "serviceRevenue.ratePerUnit" in props :
        rate = float(props["serviceRevenue.ratePerUnit"] )
    if rate==None : rate = 0
    quantity = 0
    if "serviceRevenue.numberOfUnits" in props:            
        quantity = float(props["serviceRevenue.numberOfUnits"] )
    if (quantity==None) : quantity =0                
    revenue = rate *  quantity
    return {"cost":cost ,"revenue":revenue}

def parseChargeList(tenant,chargeList,data,shipping,g):
    """
    Parsing charge to db
    """
    if chargeList != None :
        for charge in chargeList :
            spv = None
            if "serviceParty" not in charge:
                continue
            serviceParty = charge['serviceParty']
            if serviceParty != None: 
                if 'id' not in serviceParty and 'erpId' in serviceParty:
                    serviceParty['id']=serviceParty['erpId']
                serviceParty = flattenDict(serviceParty)
                spv  = addVertex('serviceParty',tenant,serviceParty,g,objectType="serviceParty")
            del charge['serviceParty']

            chargeDict = flattenDict(charge)
            monData = getMonetaryData(chargeDict)
            chargeDict['cost'] = monData['cost']
            chargeDict['revenue'] = monData['revenue']
            v  = addVertex('charge',tenant,chargeDict,g,False)
            addEdge('charge',shipping,v,g)
            if spv != None : 
                addEdge('serviceParty',v,spv,g)

def initialData(data):
    if 'tenantId' not in data:
        data['tenantId']="Oconor"
    if 'id' not in data: 
        if "erpId" in data:
            data['id']=data['erpId']
        else:
            print("No erpId and id")
            raise Exception("No erpId and id")

    if "type" not in data:
        data['type']=None
    if 'id' not in data['shipper'] and 'erpId' in data['shipper']:            
        data['shipper']['id']=data['shipper']['erpId']
    if 'id' not in data['consignee'] and 'erpId' in data['consignee']:
        data['consignee']['id']=data['consignee']['erpId']
    return data
